ros-db
======

This repository contains the dataset and R analysis script used in Dettmering _et al._ (2014). At the moment, only the data for AG1522 cells are included. The figures are different from those used in the paper.

* `doi:10.1093/jrr/rru083`
* Full citation: T. Dettmering, S. Zahnreich, M. Colindres-Rojas, M. Durante, G. Taucher-Scholz, C. Fournier. Increased effectiveness of carbon ions in the production of reactive oxygen species in normal human fibroblasts. Journal of Radiation Research, 2014.
